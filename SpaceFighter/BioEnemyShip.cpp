
#include "BioEnemyShip.h"


BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}

// Updates the location of the BioEnemy Ship
void BioEnemyShip::Update(const GameTime *pGameTime)
{
	// First checks to see if the ship is still active
	if (IsActive())
	{
		// Create a 'x' variable to hold the flight information
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		// Create the side to side motion of the bio enemy ship
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		// Check if the ship is on the screen - if it not on the screen, deactivate it
		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
